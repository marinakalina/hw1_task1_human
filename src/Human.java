public class Human {
    private String name;
    private String surname;
    private String secondName;

    public Human(String name, String surname){
        this.name = name;
        this.surname = surname;
    }

    public Human(String name, String surname, String secondName){
        this.name = name;
        this.surname = surname;
        if(secondName!=null) {
            this.secondName = secondName;
        }
    }

    public String getFullName(){
        if(secondName!=null) {
            return surname + name + secondName;
        } else {return surname + name;}
    }

    public String getShortName(){
        String shortName;
        String shortName1;
        char[] name1 = name.toCharArray();
        if(secondName!=null) {
            char[] secondName1 = secondName.toCharArray();
            shortName = surname + name1[0] + ". " + secondName1[0] + ".";
            return shortName;
        } else {
            shortName1 = surname + name1[0] + ".";
            return shortName1;
        }

    }
}
